const express = require('express')
const bodyParser = require("body-parser")

const ENV = require('./ENV.js')
const request = require('request-promise')
const Bottleneck = require('bottleneck')

const fetch = require('node-fetch').default
const FormData = require('form-data')


// Convert data to form data
const getFormData = ({ name, content }) => {
  const formData = new FormData()
  formData.append('file_names', name)
  formData.append('files', Buffer.from(content), name)
  return formData
}

// Init bottleneck limiter
const getLimiter = () =>
  new Bottleneck({
    maxConcurrent: 2,
    minTime: 1000 / 9 
  });

//Instantiations for contstructor
const setInstances = (client) => {
  client.app = express();
  client.bodyParser = new bodyParser(); 
}
class Handler {

  //Handler Constructor
  constructor() {  
    this.env = ENV
    this.limiter = getLimiter()
    this.baseUrl = 'https://api.hubapi.com'
    this.port = process.env.port || '8080'
    setInstances(this)
  }
  
  //App init method
  async initHandler(webhookData) {
    //Get paramaters from webhook data
    var webhookResults = this.webhookHandler(webhookData)
    //Upload images
    var uploadResults = this.uploadHandler(webhookResults)

    return uploadResults
  }

    //App test method
  async initTest(webhookData) {
    //Get paramaters from webhook data
    var webhookResults = this.webhookHandler(webhookData)
    //Upload images
    var uploadResults = this.uploadHandler(webhookResults)

    return uploadResults

  }
  
  //Bottleneck limiter for request-promise api calls
  requestHandler(opts) {      
    const params = opts
    return this.limiter.schedule(() => {
        request(params)
    })
  }
  
  // Read webhook, return file URL and VID
  webhookHandler(webhookData) {

    const webhookResults = []
    var property

    //Contact ID
    if(webhookData.hasOwnProperty('vid')){
      property = {
        vid: webhookData.vid
      }
      webhookResults.push(property);
    } 
    //Image One
    if(webhookData.properties.hasOwnProperty('ugc_logo')){
      property = {
        url : webhookData.properties.ugc_logo.value + '&hapikey='+ this.env.hapikey,
        name : Date.now().toString() + '-uploaded-image',
        vid: webhookData.vid
      }
      webhookResults.push(property);
    } 
    //Image Two
    if(webhookData.properties.hasOwnProperty('ugc_logo_2')){
        property = {
        url : webhookData.properties.ugc_logo_2.value + '&hapikey='+ this.env.hapikey,
        name : Date.now().toString() + '-uploaded-image',
        vid: webhookData.vid
      }
      webhookResults.push(property);
    }
    //Image Three
    if(webhookData.properties.hasOwnProperty('ugc_logo_3')){
      property = {
        url : webhookData.properties.ugc_logo_3.value + '&hapikey='+ this.env.hapikey,
        name : Date.now().toString() + '-uploaded-image',
        vid: webhookData.vid
      }
      webhookResults.push(property);
    }
    
    return webhookResults
  }

  uploadHandler(webhookResults) {
        for(var i = 1; i < webhookResults.length; ++i){
        //Index = 0 is the ID
        //Index = 1 = Image One
        if(i == 1){
          
          var fileData = webhookResults[i]
          this.uploadImageFiles(webhookResults[i])
          .then((data) => {
              //Grab uploaded image url from response/result
              const uploadResults = data.objects[0].friendly_url ? data.objects[0].friendly_url : ''
              //Combine results object with webhook object
              const contactParams = Object.assign(fileData, {uploadResults})
              //update contact property with friendly URL
              return this.updateContact(contactParams, 1)         
          })
          .catch(function (err) {
            console.log(err)
          })

        }
      //Index = 2 = Image One
        if(i == 2){
          var fileData = webhookResults[i]
          this.uploadImageFiles(webhookResults[i])
          .then((data) => {
              //Grab uploaded image url from response/result
              const uploadResults = data.objects[0].friendly_url ? data.objects[0].friendly_url : ''
              //Combine results object with webhook object
              const contactParams = Object.assign(fileData, {uploadResults})
              //update contact property with friendly URL
              return this.updateContact(contactParams, 2)         
          })
          .catch(function (err) {
            console.log(err)
          })    
        }
        //Index = 3 = Image One
        if(i > 2){
          var fileData = webhookResults[i]
          this.uploadImageFiles(webhookResults[i])
          .then((data) => {
              //Grab uploaded image url from response/result
              const uploadResults = data.objects[0].friendly_url ? data.objects[0].friendly_url : ''
              //Combine results object with webhook object
              const contactParams = Object.assign(fileData, {uploadResults})
              //update contact property with friendly URL
              return this.updateContact(contactParams, 3)         
          })
          .catch(function (err) {
            console.log(err)
          })   
        }


    }
  }


  //Upload image initializer
  async uploadImageFiles(params) {
    //Get Image, return promise
    const file = await this.grabFiles(params)    
    return file
  }

  //Download image
  async grabFiles(fileInfo) {
    
    const fetchImageResult = await fetch(fileInfo.url, {
      method: 'GET',
    })
    //console.log(fetchImageResult.body);
    //get image and store in buffer
    const content = await fetchImageResult.arrayBuffer(16)
    //Combine image object and webhook object
    const fileUploadInfo = Object.assign(fileInfo, {content})
    //Upload the file to Hubspot

      return this.uploadFile(fileUploadInfo)
  }









  //Download image
  async grabFile(fileInfo) {
    
    const fetchImageResult = await fetch(fileInfo.url, {
      method: 'GET',
    })
    //console.log(fetchImageResult.body);
    //get image and store in buffer
    const content = await fetchImageResult.arrayBuffer(16)
    //Combine image object and webhook object
    const fileUploadInfo = Object.assign(fileInfo, {content})
    //Upload the file to Hubspot
    return this.uploadFile(fileUploadInfo)
  }







  //Upload image initializer
  async uploadImageFile(params) {
    //Get Image, return promise
    const file = await this.grabFile(params)    
    return file
  }

  //Download image
  async grabFile(fileInfo) {
    
    const fetchImageResult = await fetch(fileInfo.url, {
      method: 'GET',
    })
    //console.log(fetchImageResult.body);
    //get image and store in buffer
    const content = await fetchImageResult.arrayBuffer(16)
    //Combine image object and webhook object
    const fileUploadInfo = Object.assign(fileInfo, {content})
    //Upload the file to Hubspot
    return this.uploadFile(fileUploadInfo)
  }
  
  //Upload to Hubspot
  async uploadFile(fileUploadInfo) {
    //Convert to form data
    const body = getFormData(fileUploadInfo)
    const headers = body.getHeaders()
    //Post to Hubspot, return promise
    const result = await fetch(this.env.uploadURL, {

      method: 'POST',
      body,
      headers,
    })
    return result.json()
  }
 
  //Update contact property with image URL
  updateContact(contactParameters, index) {   

    var targetProperty

    if (index == 1) {
     targetProperty = 'uploaded_image'
    } else if (index == 2) {
      targetProperty = 'uploaded_image_2'
    } else if (index == 3) {
      targetProperty = 'uploaded_image_3'
    }

    const requestOptions = this.requestHandler({
      method: 'POST',
      url: `https://api.hubapi.com/contacts/v1/contact/vid/${contactParameters.vid}/profile?hapikey=${this.env.hapikey}`,
      json: true,
      resolveWithFullResponse: true,
      body: 
         { properties: 
            [ { property: targetProperty, value: contactParameters.uploadResults }] 
         },             
    })
  }
  
}

module.exports = Handler


