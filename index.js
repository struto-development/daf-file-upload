const bodyParser = require("body-parser")
const Handler = require('./class.js')
let handler = new Handler()
//handler.app.use(bodyParser.json());

handler.app.use(bodyParser.json({
    parameterLimit: 100000,
    limit: '50mb',
    extended: true
  }));

/* Local Testing */
var fs = require("fs");
var readFile = fs.readFileSync("test.json");
var jsondata = JSON.parse(readFile);

handler.app.get('/local', (req, res) => {

//res.status(200).send('ok')
var test = handler.initTest(jsondata)


  .then(function (response) {
    // resolved
    res.status(200).send('OK')
  })
  .catch(function (err) {
    // rejected
    //console.log(err)
    res.status(500).send('Internal Server Error'+ err)
  });


});



/* App init handler */
handler.app.post('/', (req, res) => {

    handler.initHandler(req.body)
    .then(function (response) {
      // resolved
      res.status(200).send('OK')
    })
    .catch(function (err) {
      // rejected
      res.status(500).send('Internal Server Error')
    });

});

/* Debugging */
handler.app.get('/debug', (req, res) => {
  res.status(200).send('OK - Debug')  
});

/* RSS Demo */
handler.app.get('/rss', (req, res) => {
  res.sendFile('./RSS/DAFRSS_example.xml', { root: __dirname });
});


handler.app.listen(handler.port, () => {
  //Init Listner
});